@extends('layouts.master')

@section('content')
    <div class="mx-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create New Questions</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form roles="form" action="/questions" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title', '') }}" placeholder="Enter Title">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="text" class="form-control" id="content" name="content" value="{{ old('content', '') }}" placeholder="Content">
                    @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
@endsection

            