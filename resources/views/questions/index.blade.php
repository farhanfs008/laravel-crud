@extends('layouts.master')

@section('content')
    <div class="mt-2 mx-2">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Questions Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                    {{ session('success') }}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="/questions/create">Create New Question</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Questions</th>
                      <th>Content</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($questions as $key => $question)
                        <tr>
                            <td> {{ $key + 1 }}  </td>
                            <td> {{ $question->title }}  </td>
                            <td> {{ $question->content }}  </td>
                            <td style="display: flex">
                                <a href="/questions/{{$question->id}}" class="btn btn-info bt-sm" href>Show</a>
                                <a href="/questions/{{$question->id}}/edit" class="btn btn-default bt-sm" href>Edit</a>
                                <form action="/questions/{{$question->id}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" value="delete" class="btn btn-danger bt-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">No Question</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
        </div>
    </div>

@endsection