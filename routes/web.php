<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.index');
});


Route::get('/master', function() {
    return view('layouts.master');
});

Route::get('/data-tables', function() {
    return view('items.data-tables');
});

Route::get('questions', 'QuestionController@index');

Route::get('questions/create', 'QuestionController@create');
Route::post('questions', 'QuestionController@store');

Route::get('questions/{questions_id}', 'QuestionController@show');

Route::get('questions/{questions_id}/edit', 'QuestionController@edit');

Route::put('questions/{questions_id}', 'QuestionController@update');

Route::delete('questions/{questions_id}', 'QuestionController@destroy');
