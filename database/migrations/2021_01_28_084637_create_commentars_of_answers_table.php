<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentarsOfAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentars_of_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('Content');
            $table->timestamps();
            $table->unsignedBigInteger('answers_id');
            $table->unsignedBigInteger('profiles_id');

            $table->foreign('answers_id')->references('id')->on('answers');
            $table->foreign('profiles_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commentars_of_answers');
    }
}
