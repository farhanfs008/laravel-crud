<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->unsignedBigInteger('profiles_id');
            $table->unsignedBigInteger('right_answers_id');

            $table->foreign('profiles_id')->references('id')->on('profiles');
            $table->foreign('right_answers_id')->references('id')->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            dropForeign('right_answers_id');
            dropColumn('right_answers_id');
        });
    }
}
