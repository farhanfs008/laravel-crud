<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class QuestionController extends Controller
{
    public function index() {
        $questions = DB::table('questions')->get();
        return view('questions.index', compact('questions'));
    }
    
    public function create() {
        return view('questions.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $query = DB::table('questions')->insert([
            "title" => $request ["title"],
            "content" => $request ["content"]
        ]);
        return redirect('questions')->with('success','Question is saved succesfully!');
    }

    public function show($id) {
        $question = DB::table('questions')->where('id', $id)->first();
        return view('questions.show', compact('question'));
    }

    public function edit($id) {
        $question = DB::table('questions')->where('id', $id)->first();
        
        return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);
        
        $query = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'],
                        'content' => $request['content']
                    ]);
        return redirect('questions')->with('success', 'Updated!');
    }

    public function destroy($id) {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('questions')->with('success', 'Deleted!');
    }
}
